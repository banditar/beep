# Beep

Music player and piano application implemented in C++.

It is able to play Hungarian Christmas songs, and famous movie soundtracks, with the use of the most basic sound emitter on the motherboard.\
It is solely done with the help of the `Beep(herz, duration)` function from the _windows.h_ library.

It has two modes:
- music player
- piano

In the piano section, one can press the keys on their keyboard, which, in response will emit the corresponding note.\
One can shift octaves of the piano, and prolong the duration of the notes on the keyboard.

---

When the program starts, a menu is shown, where we can do 5 different things:
- listen to songs from our music library
- play the piano
- seek help on how to use the application
- look at the credits
- exit

---

This project was done as a computer science thesis, in our 12th grade at Bolyai Farkas Theoretical High School.\
Students:
- Török Csongor
- Jánosi József-Hunor
