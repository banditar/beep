#include <iostream>
#include <windows.h>
#include <conio.h>
#include <fstream>

using namespace std;

void readF();
void startF();
void menuF();
void songsF();
void pianoF();
void descriptionF();
void exitF();
void christmasF();
void soundtrackF();
void hullapelyhes();
void mennybolazangyal();
void kiskaracsony();
void szepfenyo();
void pasztorok();
void csendesely();
void pink_panther();
void star_wars();
void pirates();
void gothrones();
void harryP();
void keys();
///F = Function


int main()
{
    readF();
    startF();
    menuF();
    exitF();
    return 0;
}

char start[ 100 ][ 100 ] = { 0 },
     menu[ 100 ][ 100 ] = { 0 },
     songs[ 100 ][ 100 ] = { 0 },
     piano[ 100 ][ 100 ] = { 0 },
     description[ 100 ][ 100 ] = { 0 },
     christmas[ 100 ][ 100 ] = { 0 },
     soundtrack[ 100 ][ 100 ] = { 0 };
///TEXTS

int start_l, menu_l, songs_l, piano_l, description_l, christmas_l, soundtrack_l;///LENGHTS
float notes[ 10 ][ 12 ] = { 0 };

void readF()
{
    cout << "Please wait until I wake up";
    int napTime = 400;

    ifstream startT( "start.txt" );
    ifstream menuT( "menu.txt" );
    ifstream songsT( "songs.txt" );
    ifstream pianoT( "piano.txt" );
    ifstream descriptionT( "description.txt" );
    ifstream christmasT( "christmas.txt" );
    ifstream soundtrackT( "soundtrack.txt" );
    ifstream notesT( "notes.txt" );


    cout << '.';
    Sleep( napTime );

    startT >> start_l;
    for( int i = 0; i < start_l; i++ )
        startT.getline( start[ i ], 100 );

    cout << '.';
    Sleep( napTime );

    menuT >> menu_l;
    for( int i = 0; i < menu_l; i++ )
        menuT.getline( menu[ i ], 100 );

    cout << '.';
    Sleep( napTime );

    songsT >> songs_l;
    for( int i = 0; i < songs_l; i++ )
        songsT.getline( songs[ i ], 100 );

    cout << '.';
    Sleep( napTime );

    pianoT >> piano_l;
    for( int i = 0; i < piano_l; i++ )
        pianoT.getline( piano[ i ], 100 );

    cout << '.';
    Sleep( napTime );

    descriptionT >> description_l;
    for( int i = 0; i < description_l; i++ )
        descriptionT.getline( description[ i ], 100 );

    cout << '.';
    Sleep( napTime );

    christmasT >> christmas_l;
    for( int i = 0; i < christmas_l; i++ )
        christmasT.getline( christmas[ i ], 100 );

    cout << '.';
    Sleep( napTime );

    soundtrackT >> soundtrack_l;
    for( int i = 0; i < soundtrack_l; i++ )
        soundtrackT.getline( soundtrack[ i ], 100 );

    cout << '.';
    Sleep( napTime );

    for( int i = 0; i < 10; i++ )
        for( int j = 0; j < 12; j++ )
            notesT >> notes[ i ][ j ];

    cout << '.';
    Sleep( napTime );

    startT.close();
    menuT.close();
    songsT.close();
    pianoT.close();
    descriptionT.close();
    christmasT.close();
    soundtrackT.close();
    notesT.close();

    cout << endl << endl << "Okay, here we go";
    Sleep( napTime + 800 );
    system( "cls" );
}

void startF()
{
    for( int i = 0; i < start_l; i++ )
        cout << start[ i ] << endl;
    _getch();
    system( "cls" );
}

void menuF()
{
    for( int i = 0; i < menu_l; i++ )
        cout << menu[ i ] << endl;

    char k;

A:
    k = _getch();

    if( k == '1' )
    {
        system( "cls" );
        songsF();
        goto end;
    }
    else if( k == '2' )
    {
        system( "cls" );
        pianoF();
        goto end;
    }
    else if( k == '3' )
    {
        system( "cls" );
        descriptionF();
        goto end;
    }
    else if( k == '4' or k == '\033' )
    {
        system( "cls" );
        goto end;
    }
    goto A;
end:
    ;
}

void songsF()
{
    int napTime = 500;

    for( int i = 0; i < songs_l; i++ )
        cout << songs[ i ] << endl;

    char k;

A:
    k = _getch();

    if( k == '1' )
    {
        system( "cls" );
        christmasF();
        goto end;
    }
    else if( k == '2' )
    {
        system( "cls" );
        soundtrackF();
        goto end;
    }
    else if( k == '3' or k == '\033' )
    {
        system( "cls" );
        menuF();
        goto end;
    }
    goto A;
end:
    ;
}

void christmasF()
{
    cout << "***Merry Christmas***";
    system( "cls" );

begin:
    for( int i = 0; i < christmas_l; i++ )
        cout << christmas[ i ] << endl;

    char k;

A:
    k = _getch();

    if( k == '1' )
    {
        system( "cls" );
        hullapelyhes();
        goto begin;
    }
    else if( k == '2' )
    {
        system( "cls" );
        mennybolazangyal();
        goto begin;
    }
    else if( k == '3' )
    {
        system( "cls" );
        kiskaracsony();
        goto begin;
    }
    else if( k == '4' )
    {
        system( "cls" );
        szepfenyo();
        goto begin;
    }
    else if( k == '5' )
    {
        system( "cls" );
        pasztorok();
        goto begin;
    }
    else if( k == '6' )
    {
        system( "cls" );
        csendesely();
        goto begin;
    }
    else if( k == '7' or k == '\033' )
    {
        system( "cls" );
        menuF();
        goto end;
    }
    goto A;
end:
    ;
}

int ChrNap = 500;///take a Christmas Nap, bro

int do1=261,
    re=293,
    mi=329,
    fa=349,
    szol=392,
    la=440,
    szi=493,
    do2=523,
    re2=587,
    mi2=659,
    fa2=698,
    szol2=784;

void hullapelyhes()
{
    int i;
    i=1;

    if ( i == 1 ) cout<<"Hull a pelyhes"<<endl<<endl<< "Hull ";

    Beep ( do1, 500 ) ;

    if ( i == 1 ) cout<< "a ";

    Beep ( do1, 400 ) ;

    if ( i == 1 ) cout<< "pely";

    Beep ( szol, 500 ) ;

    if ( i == 1 ) cout<< "hes ";

    Beep ( szol, 400 ) ;

    if ( i == 1 ) cout<< "fe";

    Beep ( la, 500 ) ;

    if ( i == 1 ) cout<< "her ";

    Beep ( la, 400 ) ;

    if ( i == 1 ) cout<< "ho,"<<endl;

    Beep ( szol, 600 ) ;


    Sleep(150);



    if ( i == 1 ) cout<< "Jojj ";

    Beep ( fa, 500 ) ;

    if ( i == 1 ) cout<< "el ";

    Beep ( fa, 400 ) ;

    if ( i == 1 ) cout<< "ked";

    Beep ( mi, 500 ) ;
    ////////////////////////// |dsfhgdsfhsdfh
    if ( i == 1 ) cout<< "ves ";

    Beep ( mi, 400 ) ;

    if ( i == 1 ) cout<< "Tel";

    Beep ( re, 500 ) ;

    if ( i == 1 ) cout<< "a";

    Beep ( re, 500 ) ;

    if ( i == 1 ) cout<< "po!"<<endl;

    Beep ( do1, 600 ) ;


    Sleep(150);


    if ( i == 1 ) cout<< "Min";

    Beep ( do1, 500 ) ;

    if ( i == 1 ) cout<< "den ";

    Beep ( do1, 400 ) ;

    if ( i == 1 ) cout<< "gyer";

    Beep ( szol, 500 ) ;

    if ( i == 1 ) cout<< "mek ";

    Beep ( szol, 400 ) ;

    if ( i == 1 ) cout<< "var";

    Beep ( la, 500 ) ;

    if ( i == 1 ) cout<< "va ";

    Beep ( la, 400 ) ;

    ///////////////////////////sdfhsdfhdf

    if ( i == 1 ) cout<< "var,"<<endl;

    Beep ( szol, 600 ) ;


    Sleep(150);


    if ( i == 1 ) cout<< "Vi";

    Beep ( fa, 500 ) ;

    if ( i == 1 ) cout<< "dam ";

    Beep ( fa, 400 ) ;

    if ( i == 1 ) cout<< "e";

    Beep ( mi, 500 ) ;

    if ( i == 1 ) cout<< "nek ";

    Beep ( mi, 400 ) ;

    if ( i == 1 ) cout<< "hang";

    Beep ( re, 500 ) ;

    if ( i == 1 ) cout<< "ja ";

    Beep ( re, 400 ) ;

    if ( i == 1 ) cout<< "szall."<<endl;

    Beep ( do1, 500 ) ;


    Sleep(150);


    if ( i == 1 ) cout<< "Van ";

    Beep ( szol, 400 ) ;

    if ( i == 1 ) cout<< "zsa";

    Beep ( szol, 500 ) ;

    /////////////////////////dsfjhgdfgj

    if ( i == 1 ) cout<< "kod";

    Beep ( fa, 400 ) ;

    if ( i == 1 ) cout<< "ban ";

    Beep ( fa, 400 ) ;

    if ( i == 1 ) cout<< "min";

    Beep ( mi, 500 ) ;

    if ( i == 1 ) cout<< "den ";

    Beep ( mi, 400 ) ;

    if ( i == 1 ) cout<< "jo,"<<endl;

    Beep ( re, 500 ) ;


    Sleep(150);


    if ( i == 1 ) cout<< "Pi";

    Beep ( szol, 500 ) ;

    if ( i == 1 ) cout<< "ros ";

    Beep ( szol, 400 ) ;

    if ( i == 1 ) cout<< "al";

    Beep ( fa, 500 ) ;

    if ( i == 1 ) cout<< "ma, ";

    Beep ( fa, 400 ) ;

    if ( i == 1 ) cout<< "mo";

    Beep ( mi, 500 ) ;

    ////////////////1551asgf

    if ( i == 1 ) cout<< "gyo";

    Beep ( mi, 500 ) ;

    if ( i == 1 ) cout<< "ro,"<<endl;

    Beep ( re, 500 ) ;


    Sleep(150);


    if ( i == 1 ) cout<< "Jojj ";

    Beep ( do1, 500 ) ;

    if ( i == 1 ) cout<< "el ";

    Beep ( do1, 400 ) ;

    if ( i == 1 ) cout<< "hoz";

    Beep ( szol, 500 ) ;

    if ( i == 1 ) cout<< "zank, ";

    Beep ( szol, 400 ) ;

    if ( i == 1 ) cout<< "va";

    Beep ( la, 500 ) ;

    if ( i == 1 ) cout<< "runk ";

    Beep ( la, 400 ) ;

    if ( i == 1 ) cout<< "rad,"<<endl;

    Beep ( szol, 500 ) ;


    Sleep(150);


    if ( i == 1 ) cout<< "Ked";

    Beep ( fa, 500 ) ;

    /////////////////////sdfhsdfhsd

    if ( i == 1 ) cout<< "ves ";

    Beep ( fa, 400 ) ;

    if ( i == 1 ) cout<< "o";

    Beep ( mi, 500 ) ;

    if ( i == 1 ) cout<< "reg, ";

    Beep ( mi, 400 ) ;

    if ( i == 1 ) cout<< "Tel";

    Beep ( re, 500 ) ;

    if ( i == 1 ) cout<< "a";

    Beep ( re, 500 ) ;

    if ( i == 1 ) cout<< "po!"<<endl<<endl;

    Beep ( do1, 600 ) ;

    Sleep( ChrNap );

    system( "cls" );
}

void mennybolazangyal()
{
    int j;
    j=1;
    cout<<"Mennybol az angyal"<<endl<<endl;
    //<<"Ha szeretne dalszoveget is akkor irjon 1-et! "<<endl;
    //cin>>j;
    if(j==1)
        cout<<"Menny";
    Beep(fa,750);
    if(j==1)
        cout<<"bol ";
    Beep(fa,400);
    if(j==1)
        cout<<"az ";
    Beep(mi,400);
    if(j==1)
        cout<<"an";
    Beep(fa,750);
    if(j==1)
        cout<<"gyal ";
    Beep(do1,650);
    if(j==1)
        cout<<"le";
    Beep(la,750);
    if(j==1)
        cout<<"jott ";
    Beep(la,500);
    if(j==1)
        cout<<"hoz";
    Beep(szol,500);
    if(j==1)
        cout<<"za";
    Beep(la,750);
    if(j==1)
        cout<<"tok ";
    Beep(fa,650);
    if(j==1)
        cout<<endl;
    Sleep(300);
    if(j==1)
        cout<<"Pasz";
    Beep(la,700);
    if(j==1)
        cout<<"to";
    Beep(re2,750);
    if(j==1)
        cout<<"rok ";
    Beep(do2,700);
    Sleep(600);
    if(j==1)
        cout<<"pasz";
    Beep(la,700);
    if(j==1)
        cout<<"to";
    Beep(re2,750);
    if(j==1)
        cout<<"rok ";
    Beep(do2,700);
    Sleep(750);
    if(j==1)
        cout<<endl;
    if(j==1)
        cout<<"Hogy";
    Beep(do2,750);
    if(j==1)
        cout<<" Bet";
    Beep(do2,400);
    if(j==1)
        cout<<"le";
    Beep(re2,400);
    if(j==1)
        cout<<"hem";
    Beep(do2,750);
    if(j==1)
        cout<<"be ";
    Beep(la,600);
    Sleep(350);
    if(j==1)
        cout<<"si";
    Beep(szi,750);
    if(j==1)
        cout<<"et";
    Beep(szi,400);
    if(j==1)
        cout<<"ve ";
    Beep(do2,600);
    if(j==1)
        cout<<"men";
    Beep(szi,750);
    if(j==1)
        cout<<"ve ";
    Beep(szol,500);
    if(j==1)
        cout<<endl;
    Sleep(350);
    if(j==1)
        cout<<"Las";
    Beep(la,800);
    if(j==1)
        cout<<"sa";
    Beep(szol,850);
    if(j==1)
        cout<<"tok ";
    Beep(fa,800);
    Sleep(650);
    if(j==1)
        cout<<"las";
    Beep(la,800);
    if(j==1)
        cout<<"sa";
    Beep(szol,800);
    if(j==1)
        cout<<"tok!";
    Beep(fa,850);
    if(j==1)
        cout<<endl<<endl;
    Sleep(700);
    if(j==1)
        cout<<"Is";
    Beep(fa,750);
    if(j==1)
        cout<<"ten";
    Beep(fa,400);
    if(j==1)
        cout<<"nek ";
    Beep(mi,400);
    if(j==1)
        cout<<"fi";
    Beep(fa,750);
    if(j==1)
        cout<<"a";
    Beep(do1,650);
    if(j==1)
        cout<<" a";
    Beep(la,750);
    if(j==1)
        cout<<"ki ";
    Beep(la,500);
    if(j==1)
        cout<<"szu";
    Beep(szol,500);
    if(j==1)
        cout<<"le";
    Beep(la,750);
    if(j==1)
        cout<<"tett ";
    Beep(fa,650);
    if(j==1)
        cout<<endl;
    Sleep(300);
    if(j==1)
        cout<<"Ja";
    Beep(la,700);
    if(j==1)
        cout<<"szol";
    Beep(re2,750);
    if(j==1)
        cout<<"ban ";
    Beep(do2,700);
    Sleep(600);
    if(j==1)
        cout<<"ja";
    Beep(la,700);
    if(j==1)
        cout<<"szol";
    Beep(re2,750);
    if(j==1)
        cout<<"ban ";
    Beep(do2,700);
    Sleep(750);
    if(j==1)
        cout<<endl;
    if(j==1)
        cout<<"O";
    Beep(do2,750);
    if(j==1)
        cout<<" le";
    Beep(do2,400);
    if(j==1)
        cout<<"szen ";
    Beep(re2,400);
    if(j==1)
        cout<<"nek";
    Beep(do2,750);
    if(j==1)
        cout<<"tek ";
    Beep(la,600);
    Sleep(350);
    if(j==1)
        cout<<"ud";
    Beep(szi,750);
    if(j==1)
        cout<<"vo";
    Beep(szi,400);
    if(j==1)
        cout<<"zi";
    Beep(do2,600);
    if(j==1)
        cout<<"to";
    Beep(szi,750);
    if(j==1)
        cout<<"tok ";
    Beep(szol,500);
    if(j==1)
        cout<<endl;
    Sleep(350);
    if(j==1)
        cout<<"Va";
    Beep(la,800);
    if(j==1)
        cout<<"lo";
    Beep(szol,850);
    if(j==1)
        cout<<"ban ";
    Beep(fa,800);
    Sleep(650);
    if(j==1)
        cout<<"va";
    Beep(la,800);
    if(j==1)
        cout<<"lo";
    Beep(szol,800);
    if(j==1)
        cout<<"ban!";
    Beep(fa,850);

    Sleep( ChrNap );
    system( "cls" );
}

void kiskaracsony()
{

    cout<<"Kiskaracsony, nagykaracsony"<<endl<<endl<< "Kis";

    Beep ( szol, 450 ) ;

    cout<< "ka";

    Beep ( re, 400 ) ;

    cout<< "ra";

    Beep ( szol, 700 ) ;

    cout<< "csony,";

    Beep ( szi, 500 ) ;

    Sleep(100);

    ///////

    cout<< "nagy";

    Beep ( la, 450 ) ;

    cout<< "ka";

    Beep ( szol, 400 ) ;

    cout<< "ra";

    Beep ( la, 700 ) ;

    cout<< "csony,"<<endl;

    Beep ( la, 500 ) ;



    Sleep(200);


///////

    cout<< "Ki ";

    Beep ( szol, 400 ) ;

    cout<< "sult";

    Beep ( re, 400 ) ;

    cout<< "-e ";

    Beep ( szol, 600 ) ;

    cout<< "mar ";

    Beep ( szi, 500 ) ;

    Sleep(100);


    //////////


    cout<< "a ";

    Beep ( la, 400 ) ;

    cout<< "ka";

    Beep ( szol, 400 ) ;

    cout<< "la";

    Beep ( la, 600 ) ;

    cout<< "csom?"<<endl;

    Beep ( la, 400 ) ;

    Sleep(200);


///////

    cout<< "Ha ";

    Beep ( szi, 400 ) ;

    cout<< "ki";

    Beep ( do2, 400 ) ;

    cout<< "sult ";

    Beep ( re2, 550 ) ;

    cout<< "mar,";

    Beep ( szol, 600 ) ;

    Sleep(100);


    ///////

    cout<< "i";

    Beep ( do2, 400 ) ;

    cout<< "de ";

    Beep ( szi, 500 ) ;

    cout<< "ve";

    Beep ( la, 600 ) ;

    cout<< "le,"<<endl;

    Beep ( la, 400 ) ;

    Sleep(200);


    ///////

    cout<< "Hadd ";

    Beep ( szi,500 ) ;

    cout<< "e";

    Beep ( do2, 400 ) ;

    cout<< "gyem ";

    Beep ( re2, 550 ) ;

    cout<< "meg ";

    Beep ( szol, 400 ) ;

    Sleep(100);


    ///////

    cout<< "me";

    Beep ( szi, 400 ) ;

    cout<< "le";

    Beep ( la, 400 ) ;

    cout<< "ge";

    Beep ( szol, 600 ) ;

    cout<< "ben."<<endl;

    Beep ( szol, 400 ) ;

    Sleep( ChrNap );
    system( "cls" );
}

void szepfenyo()
{
    cout<<"O szep fenyo"<<endl<<endl<< "O ";

    Beep ( do1, 700 ) ;

    cout<< "szep  ";

    Beep ( fa, 600 ) ;

    cout<< "fe";

    Beep ( fa, 400 ) ;

    cout<< "nyo, ";

    Beep ( fa, 600 ) ;

    Sleep(100);

    cout<< "o ";

    Beep ( szol, 700 ) ;
///////////////////

    cout<< "szep ";

    Beep ( la, 600 ) ;

    cout<< "fe";

    Beep ( la, 400 ) ;

    cout<< "nyo, ";

    Beep ( la, 600 ) ;

    Sleep(200);


    cout<< "oly ";

    Beep ( la, 500 ) ;

    cout<<"ked";

    Beep ( szol, 450 ) ;
    //////////////////

    cout<< "ves ";

    Beep ( la, 400 ) ;

    Sleep(100);


    cout<< "min";

    Beep ( szi, 600 ) ;

    cout<< "den ";

    Beep ( mi, 600 ) ;

    Sleep(100);


    cout<< "a";

    Beep ( szol, 650 ) ;

    cout<< "gad!"<<endl;

    Beep ( fa, 600 ) ;
    ///////////////////
///////////////


    Sleep(200);

    cout<< "O ";

    Beep ( do1, 700 ) ;

    cout<< "szep  ";

    Beep ( fa, 600 ) ;

    cout<< "fe";

    Beep ( fa, 400 ) ;

    cout<< "nyo, ";

    Beep ( fa, 600 ) ;

    Sleep(100);

    cout<< "o ";

    Beep ( szol, 700 ) ;
///////////////////

    cout<< "szep ";

    Beep ( la, 600 ) ;

    cout<< "fe";

    Beep ( la, 400 ) ;

    cout<< "nyo, ";

    Beep ( la, 600 ) ;

    Sleep(200);


    cout<< "oly ";

    Beep ( la, 500 ) ;

    cout<<"ked";

    Beep ( szol, 450 ) ;
    //////////////////

    cout<< "ves ";

    Beep ( la, 400 ) ;

    Sleep(100);


    cout<< "min";

    Beep ( szi, 550 ) ;

    cout<< "den ";

    Beep ( mi, 550 ) ;

    Sleep(100);


    cout<< "a";

    Beep ( szol, 650 ) ;

    cout<< "gad!"<<endl;

    Beep ( fa, 600 ) ;
    ///////////////////
///////////////


    Sleep(200);

    cout<< "Te ";

    Beep ( do2, 400 ) ;

    cout<< "zold ";

    Beep ( do2, 400 ) ;

    cout<< "vagy, ";

    Beep ( la, 400 ) ;


    cout<< "mig ";

    Beep ( re2, 700 ) ;

    cout<< "a ";

    Beep ( do2, 400 ) ;
    /////////////////

    cout<< "nyar ";

    Beep ( do2, 500 ) ;

    cout<< "tu";

    Beep ( szi, 400 ) ;

    cout<< "zel, ";

    Beep ( szi, 500 ) ;

    Sleep(250);


    cout<< "es ";

    Beep ( szi, 400 ) ;

    cout<< "zold, ";

    Beep ( do2, 400 ) ;

    cout<< "ha ";

    Beep ( la, 400 ) ;

    cout<< "te";

    Beep ( re2, 700 ) ;

    cout<< "li ";

    Beep ( do2, 400 ) ;

    //////////////

    cout<< "ho ";

    Beep ( szi, 550 ) ;

    cout<< "fod ";

    Beep ( la, 400 ) ;

    cout<< "el."<<endl;
    Beep ( la, 400 ) ;


    Sleep(200);

/////////////////

    cout<< "O ";

    Beep ( do1, 700 ) ;

    cout<< "szep  ";

    Beep ( fa, 600 ) ;

    cout<< "fe";

    Beep ( fa, 400 ) ;

    cout<< "nyo, ";

    Beep ( fa, 600 ) ;

    Sleep(100);

    cout<< "o ";

    Beep ( szol, 700 ) ;
///////////////////

    cout<< "szep ";

    Beep ( la, 600 ) ;

    cout<< "fe";

    Beep ( la, 400 ) ;

    cout<< "nyo, ";

    Beep ( la, 600 ) ;

    Sleep(200);


    cout<< "oly ";

    Beep ( la, 500 ) ;

    cout<<"ked";

    Beep ( szol, 450 ) ;
    //////////////////

    cout<< "ves ";

    Beep ( la, 400 ) ;

    Sleep(100);


    cout<< "min";

    Beep ( szi, 550 ) ;

    cout<< "den ";

    Beep ( mi, 550 ) ;

    Sleep(100);


    cout<< "a";

    Beep ( szol, 650 ) ;

    cout<< "gad!"<<endl;

    Beep ( fa, 600 ) ;
    ///////////////////
///////////////
    Sleep( ChrNap );
    system( "cls" );

}

void pasztorok()
{
    cout<<"Pasztorok, pasztorok"<<endl<<endl<<"Pasz";

    Beep(do1,600);

    cout<<"to";

    Beep(mi,400);

    cout<<"rok, ";

    Beep(szol,450);
//////////////////////////////
    cout<<"pasz";

    Beep(do1,600);

    cout<<"to";

    Beep(mi,450);

    cout<<"rok ";

    Beep(szol,400);
//////////////////////////////

    cout<<"or";

    Beep(fa,450);

    cout<<"ven";

    Beep(mi,400);

    cout<<"dez";

    Beep(re,600);

    cout<<"ve,"<<endl;

    Beep(do1,550);
//////////////////////////////////
//////////////////////////////////
    Sleep(200);
    cout<<"Si";

    Beep(szol,600);

    cout<<"et";

    Beep(szi,400);

    cout<<"nek ";

    Beep(re2,400);
/////////////////

    cout<<"Je" ;

    Beep(szol,600);

    cout<<"zus";

    Beep(szi,400);

    cout<<"hoz ";

    Beep(re2,450);
/////////////////

    cout<<"Bet";

    Beep(do2,450);

    cout<<"le";

    Beep(szi,450);

    cout<<"hem";

    Beep(la,550);

    Sleep(100);

    cout<<"be."<<endl;

    Beep(szol,500);
///////////////////////
////////////////////////
    Sleep(200);
    cout<<"Ko";

    Beep(do2,600);

    cout<<"szon";

    Beep(szi,400);

    cout<<"test ";

    Beep(la,450);

    cout<<"mon";

    Beep(szol,600);

    cout<<"da";

    Beep(fa,400);

    cout<<"nak ";

    Beep(mi,450);

    cout<<"a ";

    Beep(szol,400);

    cout<<"kis";

    Beep(fa,450);

    cout<<"ded";

    Beep(mi,550);

    cout<<"nek,"<<endl;

    Beep(re,550);
/////////////////////
///////////////////////
    Sleep(200);

    cout<<"Ki ";

    Beep(do1,600);

    cout<<"valt";

    Beep(mi,400);

    cout<<"sa";

    Beep(szol,400);
//////////////////////////

    cout<<"got ";

    Beep(do1,500);

    cout<<"ho";

    Beep(mi,500);

    cout<<"zott ";

    Beep(szol,400);
//////////////////////////

    cout<<"az ";

    Beep(fa,400);

    cout<<"em";

    Beep(mi,400);

    cout<<"ber";

    Beep(re,600);

    cout<<"nek.";

    Beep(do1,550);

    Sleep( ChrNap );
    system( "cls" );
}

void csendesely()
{
    cout<<"Csendes ej"<<endl<<endl<<"Csen";
    Beep(szol,800);
    Beep(la,400);
    cout<<"des ";
    Beep(szol,800);
    cout<<"ej, ";
    Beep(mi,800);
    Sleep(800);
    cout<<"dra";
    Beep(szol,800);
    cout<<"ga ";
    Beep(la,600);
    cout<<"szent";
    Beep(szol,600);
    cout<<" ej, ";
    Beep(mi,800);
    Sleep(800);
    cout<<"min";
    Beep(re2,800);
    cout<<"de";
    Beep(re2,650);
    cout<<"nek ";
    Beep(szi,800);
    Sleep(400);
    cout<<"a'l";
    Beep(do2,850);
    cout<<"ma ";
    Beep(do2,600);
    cout<<"mely,";
    Beep(szol,800);
    cout<<endl;
    Sleep(600);
    cout<<"Nincs ";
    Beep(la,850);
    cout<<"fonn ";
    Beep(la,600);
    cout<<"mas, ";
    Beep(do2,850);
    cout<<"csak";
    Beep(szi,400);
    cout<<" a ";
    Beep(la,400);
    cout<<"dra";
    Beep(szol,850);
    cout<<"ga";
    Beep(la,400);
    cout<<" szent";
    Beep(szol,600);
    cout<<" par,";
    Beep(mi,800);
    Sleep(400);
    cout<<" var";
    Beep(la,850);
    cout<<"ja ";
    Beep(la,400);
    cout<<"gyer";
    Beep(do2,800);
    cout<<"me";
    Beep(szi,500);
    cout<<"ke ";
    Beep(la,500);
    cout<<"al";
    Beep(szol,800);
    cout<<"szik";
    Beep(la,400);
    cout<<"-e";
    Beep(szol,400);
    cout<<" mar";
    Beep(mi,850);
    cout<<endl;
    Sleep(600);
    cout<<"Kuldj";
    Beep(re2,850);
    cout<<" le";
    Beep(re2,550);
    cout<<" ra";
    Beep(re2,700);
    cout<<" al";
    Beep(fa2,900);
    cout<<"mot ";
    Beep(re2,500);
    cout<<"nagy";
    Beep(szi,850);
    cout<<" eg!";
    Beep(do2,800);
    Beep(mi2,700);
    Sleep(800);
    cout<<" Kuldj";
    Beep(do2,850);
    cout<<" le";
    Beep(szol,550);
    cout<<" ra";
    Beep(mi,700);
    cout<<" al";
    Beep(szol,750);
    cout<<"mot";
    Beep(fa,500);
    cout<<" nagy";
    Beep(re+20,600);
    cout<<" eg!";
    Beep(do1,900);

    Sleep( ChrNap );
    system( "cls" );
}

void soundtrackF()
{
begin:
    for( int i = 0; i < soundtrack_l; i++ )
        cout << soundtrack[ i ] << endl;

    char k;

A:
    k = _getch();

    if( k == '1' )
    {
        system( "cls" );
        pink_panther();
        goto begin;
    }
    else if( k == '2' )
    {
        system( "cls" );
        star_wars();
        goto begin;
    }
    else if( k == '3' )
    {
        system( "cls" );
        pirates();
        goto begin;
    }
    else if( k == '4' )
    {
        system( "cls" );
        gothrones();
        goto begin;
    }
    else if( k == '5' )
    {
        system( "cls" );
        harryP();
        goto begin;
    }
    else if( k == '6' or k == '\033' )
    {
        system( "cls" );
        menuF();
        goto end;
    }
    goto A;
end:
    ;
}

void pink_panther()
{
    cout << "      \\    /\\" << endl;
    cout << "       )  ( ')" << endl;
    cout << "      (  /  )" << endl;
    cout << "       \(__)|" << endl;
    Beep(re,200);
    Beep(mi,500);
    Sleep(500);
    Beep(fa,200);
    Beep(szol,500);
    Sleep(500);
    Beep(re,93);
    Beep(mi,375);
    Beep(fa,250);
    Beep(szol,375);
    Beep(do2,250);
    Beep(szi,375);
    Beep(mi,250);
    Beep(szol,350);
    Beep(szi,250);
    Beep(szi,700);
    Sleep(100);
    Beep(la,100);
    Beep(szol,150);
    Beep(mi,200);
    Beep(re,200);
    Beep(mi,500);

    system( "cls" );
}

void star_wars()
{
    cout << "    __.-._" << endl;
    cout << "    '-._\"7'" << endl;
    cout << "     /'.-c" << endl;
    cout << "     |  /T" << endl;
    cout << "    _)_/LI" << endl;

    int Do=261.63;
    int Re=293.66;
    int Mi=329.63;
    int Fa=349.23;
    int So=392;
    int La=440;
    int Ti=493.88;
    int Do2=523.25;
    int Re2=587.33;
    int Mi2=659.26;
    int Fa2=698.46;
    int So2=783.99;

    Beep(Mi,500);
    Beep(Mi,500);
    Beep(Mi,500);
    Beep(Do,400);
    Beep(So,200);
    Beep(Mi,500);
    Beep(Do,400);
    Beep(So,200);
    Beep(Mi,500);
    Sleep(400);

    Beep(Ti,500);
    Beep(Ti,500);
    Beep(Ti,500);
    Beep(Do2,400);
    Beep(So,200);
    Beep(Mi,500);
    Beep(Do,400);
    Beep(So,200);
    Beep(Mi,500);
    Sleep(400);

    Beep(Mi2,500);
    Beep(Mi,250);
    Beep(Mi,250);
    Beep(Mi2,500);
    Beep(Mi2,250);
    Beep(Re2,250);
    Beep(Do2,133);
    Beep(Ti,133);
    Beep(Do2,500);
    Sleep(200);
    Beep(Mi,500);
    Beep(La,500);
    Beep(So,500);
    Beep(Fa,250);
    Beep(So,500);
    Beep(Do,500);
    Beep(Mi,500);
    Beep(Do,250);
    Beep(Mi,250);
    Beep(So,500);
    Beep(Mi,500);
    Beep(So,500);
    Beep(Ti,500);

    Beep(Mi2,500);
    Beep(Mi,250);
    Beep(Mi,250);
    Beep(Mi2,500);
    Beep(Mi2,250);
    Beep(Re2,250);
    Beep(Do2,133);
    Beep(Ti,133);
    Beep(Do2,500);
    Sleep(200);
    Beep(Mi,500);
    Beep(La,500);
    Beep(So,500);
    Beep(Fa,250);
    Beep(So,500);
    Beep(Do,500);
    Beep(Mi,500);
    Beep(Do,500);
    Beep(So,200);
    Beep(Mi,500);
    Beep(Do,400);
    Beep(So,200);
    Beep(Mi,500);

    system( "cls" );
}

void pirates()
{
    cout << "     __/\\__" << endl;
    cout << "  ~~~\\____/~~~~~~" << endl;
    cout << "    ~  ~~~   ~.";


    int do1=261.63;
    int re=293.66;
    int mi=329.63;
    int fa=349.23;
    int szol=392;
    int la=440;
    int szi=493.88;
    int do2=523.25;

    Beep(220,200);
    Beep(do1,200);
    Beep(re,400);
    Beep(re,400);
    Beep(re,200);
    Beep(mi,200);
    Beep(fa,400);
    Beep(fa,400);
    Beep(fa,200);
    Beep(fa,400);
    Beep(szol,200);
    Beep(mi,400);
    Beep(mi,400);
    Beep(re,200);
    Beep(do1,200);
    Beep(do1,200);
    Beep(re,400);

    Sleep(800);

    Beep(220,200);
    Beep(do1,200);
    Beep(re,400);
    Beep(re,400);
    Beep(re,200);
    Beep(mi,200);
    Beep(fa,400);
    Beep(fa,400);
    Beep(fa,200);
    Beep(fa,400);
    Beep(szol,200);
    Beep(mi,400);
    Beep(mi,400);
    Beep(re,200);
    Beep(do1,200);
    Beep(re,400);

    Sleep(800);

    Beep(220,200);
    Beep(do1,200);
    Beep(re,400);
    Beep(re,400);
    Beep(re,200);
    Beep(fa,200);
    Beep(szol,400);
    Beep(szol,400);
    Beep(szol,200);
    Beep(la,200);
    Beep(466.16,400);
    Beep(466.16,400);
    Beep(la,200);
    Beep(szol,200);
    Beep(la,200);
    Beep(re,400);

    Sleep(350);

    Beep(re,200);
    Beep(mi,200);
    Beep(fa,400);
    Beep(fa,400);
    Beep(szol,400);
    Beep(la,200);
    Beep(re,400);

    Sleep(350);

    Beep(re,200);
    Beep(fa,200);
    Beep(mi,400);
    Beep(mi,400);
    Beep(fa,200);
    Beep(re,200);
    Beep(mi,500);

    system( "cls" );
}

void gothrones()
{
    cout << "  i______i" << endl;
    cout << "  I______I" << endl;
    cout << "  I      I" << endl;
    cout << "  I______I" << endl;
    cout << " /      /I" << endl;
    cout << "(______( I" << endl;
    cout << "I I    I I" << endl;
    cout << "I      I";

    int do1=261.63;
    int re=293.66;
    int mi=329.63;
    int fa=349.23;
    int szol=392;
    int la=440;
    int szi=493.88;
    int do2=523.25;

    Beep(do1,150);
    Beep(311.13,100);
    Beep(fa,175);
    Beep(szol,300);
    Sleep(100);
    Beep(do1,300);
    Beep(311.13,100);
    Beep(fa,175);
    Beep(szol,300);
    Sleep(100);
    Beep(do1,300);
    Beep(311.13,100);
    Beep(fa,175);
    Beep(szol,300);
    Sleep(100);
    Beep(do1,300);
    Beep(311.13,100);
    Beep(fa,175);
    Beep(szol,700);
    Sleep(500);
    Beep(do1,300);
    Beep(311.13,100);
    Beep(174.61,750);
    Beep(116.54,750);
    Beep(155.56,100);
    Beep(146.83,200);
    Beep(164.81,500);
    Beep(116.54,750);
    Beep(155.56,100);
    Beep(146.83,200);
    Beep(130.81,300);

    Beep(174.61,400);
    Beep(207.65,100);
    Beep(233.08,100);
    Beep(do1,400);
    Sleep(200);
    Beep(174.61,400);
    Beep(207.65,100);
    Beep(233.08,100);
    Beep(do1,400);
    Sleep(200);
    Beep(174.61,400);
    Beep(207.65,100);
    Beep(233.08,100);
    Beep(do1,400);
    Sleep(200);
    Beep(174.61,400);
    Beep(207.65,400);
    Beep(szol,750);
    Beep(do1,750);
    Beep(311.13,100);
    Beep(fa,200);
    Beep(szol,500);
    Beep(do1,750);
    Beep(311.13,100);
    Beep(fa,200);
    Beep(re,500);

    Beep(196.00,400);
    Beep(233.08,100);
    Beep(do1,175);
    Beep(re,400);
    Sleep(200);

    Beep(196.00,400);
    Beep(233.08,100);
    Beep(do1,175);
    Beep(re,400);
    Sleep(200);

    Beep(196.00,400);
    Beep(233.08,100);
    Beep(do1,175);
    Beep(re,400);
    Sleep(200);

    Beep(196.00,400);
    Beep(233.08,100);
    Beep(do1,175);
    Beep(re,400);
    Sleep(200);

    Beep(196.00,400);
    Beep(233.08,100);
    Beep(do1,175);
    Beep(re,400);
    Sleep(200);

    system( "cls" );
}

void harryP()
{
    cout << " (o,o)" << endl;
    cout << "<  .  >" << endl;
    cout << "--\"-\"---";

    int do1=261.63;
    int re=293.66;
    int mi=329.63;
    int fa=349.23;
    int szol=392;
    int la=440;
    int szi=493.88;
    int do2=523.25;


    Beep(mi,500);
    Beep(szol,166);
    Beep(fa,333);
    Beep(mi,666);
    Beep(szi,333);
    Beep(la,900);
    Beep(fa,900);

    Beep(mi,500);
    Beep(szol,166);
    Beep(fa,333);

    Beep(re,666);
    Beep(fa,333);
    Beep(246.94,900);

    Sleep(300);

    Beep(246.94,600);
    Beep(246.94,333);

    Beep(mi,500);
    Beep(szol,166);
    Beep(fa,333);

    Beep(mi,666);
    Beep(szi,333);
    Beep(587.33,666);
    Beep(587.33,333);
    Beep(szi,666);
    Beep(la,333);
    Beep(fa,500);
    Beep(szi,166);
    Beep(la,333);
    Beep(szol,333);

    system( "cls" );
}

void keys( int oktav, int time )
{
    system( "cls" );

    cout << "   A#" << oktav - 1 << "       C#" << oktav - 1 << "  Eb" << oktav << "       F#" << oktav << "  G#" << oktav << "  A#" << oktav << "       C#" << oktav + 1 << endl;
    cout << "    W " <<              "        R " <<             "   T " <<         "        U " <<         "   I " <<         "   O " <<         "        {" << endl << endl;

    cout << " A" << oktav - 1 << "   B" << oktav - 1 << "   C" << oktav << "   D" << oktav << "   E" << oktav << "   F" << oktav << "   G" << oktav << "   A" << oktav << "   B" << oktav << "   C" << oktav + 1 << "   D" << oktav + 1 << endl;
    cout << " A" <<             "    S" <<             "    D" <<         "    F" <<         "    G" <<         "    H" <<         "    J" <<         "    K" <<         "    L" <<         "    :" <<         "    \"" << endl << endl;

    cout << endl << "Current timelength:" << time << endl;
    cout << "Current main octav:" << oktav << endl;

    cout << endl << "Press <UP ARROW KEY> to increase pitch with an octav" << endl;
    cout <<       "Press <DOWN ARROW KEY> to decrease pitch with an octav" << endl << endl;

    cout << "Press <LEFT ARROW> to slow down the rhytm by 50 miliseconds" << endl;
    cout << "Press <RIGHT ARROW> to speed up the rythm by 50 miliseconds" << endl << endl;

    cout << "Press <ESCAPE> to get back to the Menu" << endl << endl;
}

void pianoF()
{
    for( int i = 0; i < piano_l; i++ )
        cout << piano[ i ] << endl;

    _getch();

    int
    C = 0,
    Cd = 1,
    D = 2,
    Eb = 3,
    E = 4,
    F = 5,
    Fd = 6,
    G = 7,
    Gd = 8,
    A = 9,
    Bb = 10,
    B = 11;

    int oktav = 4;
    float time = 200;

    keys( oktav, time );
    char k;
    do
    {
        k = _getch();

        if( k == 72 )///Up arrow to upp an octav
        {
            if( oktav < 8 )
                keys( ++oktav, time );
        }
        else if( k == 80 )///down arrow to decrease an octav
        {
            if( oktav > 1 )
                keys( --oktav, time );
        }
        else if( k == 75 )///Slow down
        {
            if( time > 0 )
            {
                time -= 50;
                keys( oktav, time );
            }
        }
        else if( k == 77 )///Speed up
        {
            time += 50;
            keys( oktav, time );
        }

        else if( k == 'a' or k == 'A' )
            Beep( notes[ oktav - 1 ][ A ], time );

        else if( k == 'w' or k == 'W' )
            Beep( notes[ oktav - 1 ][ Bb ], time );

        else if( k == 's' or k == 'S' )
            Beep( notes[ oktav - 1 ][ B ], time );

        else if( k == 'd' or k == 'D' )
            Beep( notes[ oktav ][ C ], time );

        else if( k == 'r' or k == 'R' )
            Beep( notes[ oktav ][ Cd ], time );

        else if( k == 'f' or k == 'F' )
            Beep( notes[ oktav ][ D ], time );

        else if( k == 't' or k == 'T' )
            Beep( notes[ oktav ][ Eb ], time );

        else if( k == 'g' or k == 'G' )
            Beep( notes[ oktav ][ E ], time );

        else if( k == 'h' or k == 'H' )
            Beep( notes[ oktav ][ F ], time );

        else if( k == 'u' or k == 'U' )
            Beep( notes[ oktav ][ Fd ], time );

        else if( k == 'j' or k == 'J' )
            Beep( notes[ oktav ][ G ], time );

        else if( k == 'i' or k == 'I' )
            Beep( notes[ oktav ][ Gd ], time );

        else if( k == 'k' or k == 'K' )
            Beep( notes[ oktav ][ A ], time );

        else if( k == 'o' or k == 'O' )
            Beep( notes[ oktav ][ Bb ], time );

        else if( k == 'l' or k == 'L' )
            Beep( notes[ oktav ][ B ], time );

        else if( k == ';' or k == ':' )
            Beep( notes[ oktav + 1 ][ C ], time );

        else if( k == '[' or k == '{' )
            Beep( notes[ oktav + 1 ][ Cd ], time );

        else if( k == '\'' or k == '"' )
            Beep( notes[ oktav + 1 ][ D ], time );
    }
    while( k != '\033' );

    system( "cls" );
    menuF();
}

void descriptionF()
{
    for( int i = 0; i < description_l; i++ )
        cout << description[ i ] << endl;

    _getch();
    system( "cls" );
    menuF();
}

void exitF()
{
A:
    cout << "Are you 100% sure, you want to exit?" << endl;
    cout << "1)No" << endl;
    cout << "2)Yes";
begin:
    char k = _getch();
    if( k == '1' or k == '\033' )
    {
        system( "cls" );
        menuF();
        goto A;
        goto end;
    }
    else if( k == '2' )
    {
        system( "cls" );
        goto end;
    }
    goto begin;
end:
    ;
}
